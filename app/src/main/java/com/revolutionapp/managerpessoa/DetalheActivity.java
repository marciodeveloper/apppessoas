package com.revolutionapp.managerpessoa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.revolutionapp.managerpessoa.model.Usuario;
import com.revolutionapp.managerpessoa.util.Constantes;
import com.revolutionapp.managerpessoa.util.MaskEditUtil;

public class DetalheActivity extends AppCompatActivity implements View.OnClickListener{


    EditText edtNome;
    EditText edtCpf;
    Button btnListView2;
    Button btnListView2Del;
    Usuario userEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);

        this.edtNome = findViewById(R.id.edtTxtListView2);
        this.edtCpf = findViewById(R.id.edtTxt2ListView2);
        this.edtCpf.addTextChangedListener(MaskEditUtil.mask(this.edtCpf, MaskEditUtil.FORMAT_CPF));
        this.btnListView2 = findViewById(R.id.btnListView2);
        this.btnListView2Del = findViewById(R.id.btnListView2Del);
        this.btnListView2.setOnClickListener(this);
        this.btnListView2Del.setOnClickListener(this);
        this.varificaIntent();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id == R.id.btnListView2){

            Usuario user = new Usuario();

            if(this.edtNome.getText().toString() != "" && this.edtNome.getText().toString() != null && this.userEdit == null) {

                user.setNome(this.edtNome.getText().toString());
                user.setCpf(this.edtCpf.getText().toString());
                Intent intent = new Intent(DetalheActivity.this, MainActivity.class);
                intent.putExtra("response", user);
                setResult(RESULT_OK,intent);
                finish();

            }else if(this.edtNome.getText().toString() != "" && this.edtNome.getText().toString() != null && this.userEdit != null){

                user.setNome(this.edtNome.getText().toString());
                user.setCpf(this.edtCpf.getText().toString());
                Intent intent = new Intent(DetalheActivity.this, MainActivity.class);
                intent.putExtra("userEditResponse", user);
                setResult(Constantes.ON_RESULT_EDIT, intent);
                finish();
            }

        }else if(id == R.id.btnListView2Del && this.userEdit != null){

            Intent intent = new Intent(DetalheActivity.this, MainActivity.class);
            setResult(Constantes.ON_RESULT_DELETE,intent);
            finish();

        }
    }


    private void varificaIntent() {

        Intent intent = getIntent();
        this.userEdit = (Usuario) intent.getSerializableExtra("userEdit");

        if(this.userEdit != null){

            this.edtNome.setText(this.userEdit.getNome());
            this.edtCpf.setText(this.userEdit.getCpf());
            this.btnListView2.setText("EDITAR");
            this.btnListView2Del.setVisibility(View.VISIBLE);

        }else{

            this.btnListView2.setText("CADASTRAR");
            this.btnListView2Del.setVisibility(View.INVISIBLE);

        }

    }
}
