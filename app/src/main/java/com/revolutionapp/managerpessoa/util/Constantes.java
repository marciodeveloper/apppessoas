package com.revolutionapp.managerpessoa.util;

public class Constantes {

    public static final int ON_RESULT_REQUEST = 1;
    public static final int ON_RESULT_RESPONSE = 2;
    public static final int ON_REQUEST_EDIT = 3;
    public static final int ON_RESULT_EDIT = 4;
    public static final int ON_RESULT_DELETE = 5;

}
