package com.revolutionapp.managerpessoa.model;

import java.io.Serializable;

public class Usuario implements Serializable {

    private Long id;
    private String nome;
    private String cpf;

    public Usuario(Long id, String nome, String cpf) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
    }

    public Usuario() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCpf() {
        return cpf;
    }

    @Override
    public String toString(){
        return "Nome :"+this.nome+"\n"+"CPF :"+this.cpf;
    }
}
